<?php

namespace Drupal\eu_cookie_compliance_block\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form for managing cookie consent.
 */
class CookieManagerForm extends FormBase {

  /**
   * A configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Settings of the eu_cookie_compliance module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * An entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   A configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   An entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack.
   */
  public function __construct(
    ConfigFactory $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack) {

    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('eu_cookie_compliance.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eu_cookie_compliance_block_consent_manager';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    string $description = '') {

    if (!empty($description)) {
      $form['description'] = [
        '#type' => 'markup',
        '#markup' => $description,
        '#prefix' => '<div class="cookie-manager-form-description">',
        '#suffix' => '</div>',
      ];
    }

    $method = $this->config->get('method')
      ? $this->config->get('method') : 'default';

    if ($method === 'categories') {
      $form = $this->buildCategoriesForm($form, $form_state);
    }
    else {
      $form['not_implemented'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<p><em>The cookie consent method used by this site is not yet implemented by this manager.</em></p>'),
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $method = $this->config->get('method')
      ? $this->config->get('method') : 'default';

    if ($method === 'categories') {
      $cookie_categories = $this
        ->entityTypeManager
        ->getStorage('cookie_category')
        ->getCookieCategories();
      $cookie_categories = array_keys($cookie_categories);

      $cookie_name = $this->config->get('cookie_name');
      if (empty($cookie_name)) {
        $cookie_name = 'cookie-agreed';
      }

      $agreed_categories = [];
      foreach ($cookie_categories as $cid) {
        $consent = $form_state->getValue($cid);
        if (!empty($consent)) {
          $agreed_categories[] = $cid;
        }
      }

      $this->setCookie($cookie_name, 2);
      $this->setCookie($cookie_name . '-categories',
        json_encode($agreed_categories));
    }
  }

  /**
   * Build a form for managing consent with the categories method.
   */
  protected function buildCategoriesForm(
    array $form,
    FormStateInterface $form_state) {

    $cookie_categories = $this
      ->entityTypeManager
      ->getStorage('cookie_category')
      ->getCookieCategories();
    $cookies = $this->requestStack->getCurrentRequest()->cookies;
    $cookie_name = $this->config->get('cookie_name');
    if (empty($cookie_name)) {
      $cookie_name = 'cookie-agreed';
    }
    $agreed_cookies = [];
    $agreed_cookies_raw = $cookies->get($cookie_name . '-categories');
    if ($agreed_cookies_raw) {
      $agreed_cookies = json_decode($agreed_cookies_raw);
    }

    foreach ($cookie_categories as $cid => $category) {
      $default_value = NULL;
      if ($category['checkbox_default_state'] === 'required'
        || in_array($cid, $agreed_cookies)) {

        $default_value = 1;
      }
      $form[$cid . '_wrapper'] = [
        '#type' => 'container',
        $cid => [
          '#type' => 'checkbox',
          '#title' => $category['label'],
          '#disabled' => ($category['checkbox_default_state'] === 'required'),
          '#default_value' => $default_value,
          '#name' => $cid,
        ],
        'description' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $category['description'],
        ],
      ];
    }

    return $form;
  }

  /**
   * Set a eu_cookie_compliance cookie.
   *
   * A cookie set through this function automatically uses the expires, path and
   * domain values configured by the eu_cookie_compliance module.
   *
   * @param string $name
   *   The name of the cookie.
   * @param mixed $value
   *   The value of the cookie.
   */
  protected function setCookie(string $name, $value) {
    $domain = $this->config->get('domain') ?? '';

    $path = $this->config->get('domain_all_sites') ? '/' : '';

    $lifetime_days = (int) ($this->config->get('cookie_lifetime') ?? 100);
    $request_time = $this->requestStack->getCurrentRequest()->server
      ->get('REQUEST_TIME');
    $expire = $request_time + ($lifetime_days * 86400);

    setcookie($name, $value, $expire, $path, $domain);
  }

}
